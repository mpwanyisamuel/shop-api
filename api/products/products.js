const express = require('express');
const router = express.Router();
const getProducts = require('./controllers/getAll');
const addNewProduct = require('./controllers/add');
const getAProduct = require('./controllers/getAProduct');
const updateProduct = require('./controllers/update');
const deleteProduct = require('./controllers/delete');

router.get('/', getProducts);
router.post('/', addNewProduct);
router.get('/:productId', getAProduct);
router.patch('/:productId', updateProduct);
router.delete('/:productId', deleteProduct);

module.exports = router;