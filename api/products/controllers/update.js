const updateProduct = (req, res, next) => {
    const id = req.params.productId;

    res.status(200).json({
        message: 'updated product',
        id
    })
};

module.exports = updateProduct;