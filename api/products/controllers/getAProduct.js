const updateAProduct = (req, res, next) => {
    const id = req.params.productId;

    if (id === 'special') {
        res.status(200).json({
            message: 'Special ID',
            id
        })
    } else {
        res.status(200).json({
            message: 'You passed an id'
        })
    }
};

module.exports = updateAProduct;