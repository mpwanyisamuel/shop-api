const addNewProduct = (req, res, next) => {
    res.status(201).json({
        message: 'POST request to add new product'
    });
};

module.exports = addNewProduct;
