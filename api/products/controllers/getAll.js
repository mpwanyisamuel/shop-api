const getProducts = (req, res, next) => {
    res.status(200).json({
        message: 'Get requests to products'
    })
}

module.exports = getProducts;