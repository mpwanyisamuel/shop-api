const express = require('express');
const router = express.Router();

const getOrders = require('./controllers/getOrders');

router.get('/', getOrders);

module.exports = router;
