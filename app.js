const express = require('express');
const app = express();
const morgan = require('morgan');

// Routes
const productRoutes = require('./api/products/products');
const orders = require('./api/orders/orders');

// Logging requests to the server
app.use(morgan('dev'));

app.use('/products', productRoutes);
app.use('/orders', orders);


// Catch 404 error
app.use((req, res, next) => {
    const error = new Error('Page not found');
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    });
});

module.exports = app;
